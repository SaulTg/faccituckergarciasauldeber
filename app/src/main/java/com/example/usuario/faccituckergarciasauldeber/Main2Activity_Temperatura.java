package com.example.usuario.faccituckergarciasauldeber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity_Temperatura extends AppCompatActivity {
    Button CONERTIR= null;
    EditText CANTIDAD = null;
    TextView RESULTADO = null;
    Spinner SPINCL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2__temperatura);
        CONERTIR = (Button) findViewById(R.id.btntem);
        CANTIDAD = (EditText) findViewById(R.id.cant);
        RESULTADO =(TextView) findViewById(R.id.Result);
        SPINCL = (Spinner) findViewById(R.id.spinla);

        String [] op = {"SELECCIONA UNA OPCION", "°C A °F", "°F a °C"};

        ArrayAdapter<String> adapter = new
                ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, op);
        SPINCL.setAdapter(adapter);

        CONERTIR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CANTIDAD.getText().toString().equals("")){
                    Toast msg=Toast.makeText(getApplicationContext(),"ESCRIBA UNA CANTIDAD", Toast.LENGTH_SHORT);
                    msg.show();
                }else{
                    Double c = Double.parseDouble(CANTIDAD.getText().toString());
                    Double res = null;
                    int select = SPINCL.getSelectedItemPosition();

                    switch (select){
                        case 0:
                            res= 0.0;
                            Toast.makeText(getApplicationContext(), "SELECCIONE UNA OPCION", Toast.LENGTH_SHORT).show();
                            break;

                        case 1:
                            res= 1.8 * c + 32;

                        case 2:
                            res= (c -32 ) / 1.8;
                            break;
                    }

                    RESULTADO.setText(res.toString());
                }
            }
        });
    }
}
